package models

type Response struct {
	Code    string `json:"code"`
	Data    string `json:"data"`
	Message string `json:"message"`
}
