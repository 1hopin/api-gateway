package main

import (
	"api-gateway/routers"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {

	//Start Setup Routers
	router := gin.New()
	router.Use(gin.Recovery())

	port := os.Getenv("SERVER_PORT")
	routers.SetupRouter(router)
	log.Fatal(router.Run(":" + port))
}
