package libraries

import (
	"log"

	json "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
)

const (
	InternalServerError = 500
	InvalidRequest      = 400
	Success             = 200
)

var httpClient *fasthttp.Client
var parser json.API

func init() {
	parser = json.ConfigFastest
	httpClient = &fasthttp.Client{
		MaxConnsPerHost: 1024,
	}
}

func RequestHttp(method string, uri string, headers [][]string, contentType string, body []byte, v interface{}) (statusCode int, bytes []byte, err error) {
	req := fasthttp.AcquireRequest()
	req.SetBody(body)
	for _, header := range headers {
		req.Header.Set(header[0], header[1])
	}
	req.Header.SetMethod(method)
	req.Header.SetContentType(contentType)
	req.SetRequestURI(uri)
	res := fasthttp.AcquireResponse()
	if err = httpClient.Do(req, res); err != nil {
		return InternalServerError, nil, err
	}
	fasthttp.ReleaseRequest(req)

	bytes = res.Body()
	err = parser.Unmarshal(bytes, &v)
	if err != nil {
		log.Fatalf("JSON parse failed - %v", string(bytes))
		return InternalServerError, nil, err
	}

	return res.StatusCode(), bytes, nil
}
