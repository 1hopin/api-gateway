package libs

func DumpMap(space string, m map[string]interface{}) {
	for _, v := range m {
		if mv, ok := v.(map[string]interface{}); ok {
			//fmt.Printf("{ \"%v\": \n", k)
			DumpMap(space+"\t", mv)
			//fmt.Printf("}\n")
		}
		// else {
		// 	fmt.Printf("%v %v : %v\n", space, k, v)
		// }
	}
}
