package libs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"api-gateway/constants"

	"github.com/gin-gonic/gin"
)

type RoutesPathS struct {
	AuthRequired bool          `json:"auth_required"`
	Routes       []RoutesDataS `json:"routes"`
}

type RoutesDataS struct {
	Service string `json:"service"`
	Path    string `json:"path"`
	Method  string `json:"method"`
}

var RoutesPath RoutesPathS
var RoutesData RoutesDataS

func GetDataConfig(c *gin.Context, fileName string) (int, interface{}) {
	filePath := fmt.Sprintf("%s%s.json", "./spi_configurations/", fileName)
	jsonFile, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &RoutesPath)

	statusCode, jsonMap := GetDataFromURL(c)

	return statusCode, jsonMap
}

func GetDataFromURL(c *gin.Context) (int, interface{}) {
	var jsonMap = make(map[string]interface{})
	jsonMap["code"] = constants.InvalidRequest
	jsonMap["message"] = constants.InvalidRequest
	var statusCode int = http.StatusBadRequest

	payload, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return statusCode, jsonMap
	}

	var headers = []string{"Authorization"}
	for _, route := range RoutesPath.Routes {
		if RoutesPath.AuthRequired {
			if !CheckEmptyToken(c) {
				return statusCode, jsonMap
			}
			headers = append(headers, "Token")
		}

		statusCodeRoute, jsonStr := GetData(c, route.Service, route.Path, headers, route.Method, constants.ApplicationJSON, payload)

		if statusCodeRoute != http.StatusOK {
			return statusCode, jsonMap
		}

		json.Unmarshal([]byte(jsonStr), &jsonMap)
		DumpMap("", jsonMap)
		statusCode = statusCodeRoute
	}
	return statusCode, jsonMap
}

func CheckEmptyToken(c *gin.Context) bool {
	tokenString := c.Request.Header.Get("Token")
	token := strings.TrimPrefix(tokenString, "Basic ")
	if token == "" {
		return false
	}

	return true
}
