package libs

import (
	"fmt"
	"os"

	"api-gateway/libraries"

	"github.com/gin-gonic/gin"
)

func GetData(c *gin.Context, service string, path string, headersString []string, method string, ApplicationType string, body []byte) (int, string) {
	var baseUrl string
	switch service {
	case "customer":
		baseUrl = os.Getenv("CUSTOMER_URL")
	}
	var url string = fmt.Sprintf("%s%s", baseUrl, path)

	var headers [][]string
	for _, v := range headersString {
		s := c.Request.Header.Get(v)
		headers = append(headers, []string{v, s})
	}

	statusCode, bodyBytes, err := libraries.RequestHttp(method, url, headers, ApplicationType, []byte(body), nil)

	if err != nil {
		fmt.Printf("Call Error >>>> %+v\n", err)
	}

	responseString := string(bodyBytes)
	fmt.Printf("Response from %s >>>> %+v\n", url, responseString)

	return statusCode, responseString
}
