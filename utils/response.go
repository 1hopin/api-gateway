package utils

import "github.com/gin-gonic/gin"

type commonResponse struct {
	Code    string      `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

func RespondWithJSON(c *gin.Context, httpStatus int, code string, message string, payload interface{}) {
	c.AbortWithStatusJSON(httpStatus, commonResponse{
		Code:    code,
		Data:    payload,
		Message: message,
	})
}

func RespondWithPayload(c *gin.Context, httpStatus int, payload interface{}) {
	c.AbortWithStatusJSON(httpStatus, payload)
}
