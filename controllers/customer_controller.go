package controllers

import (
	"api-gateway/libs"

	"api-gateway/utils"

	"github.com/gin-gonic/gin"
)

type CustomerController interface {
	OtpRequest(c *gin.Context)
	OtpValidate(c *gin.Context)
	CustomerRegister(c *gin.Context)
}

type customerController struct {
}

func InitCustomerController() CustomerController {
	return &customerController{}
}

func (controller *customerController) OtpRequest(c *gin.Context) {
	statusCode, jsonMap := libs.GetDataConfig(c, "customer_v1_otps_request")
	utils.RespondWithPayload(c, statusCode, jsonMap)
}

func (controller *customerController) OtpValidate(c *gin.Context) {
	statusCode, jsonMap := libs.GetDataConfig(c, "customer_v1_otps_validate")
	utils.RespondWithPayload(c, statusCode, jsonMap)
}

func (controller *customerController) CustomerRegister(c *gin.Context) {
	statusCode, jsonMap := libs.GetDataConfig(c, "customer_v1_customers_register")
	utils.RespondWithPayload(c, statusCode, jsonMap)
}
