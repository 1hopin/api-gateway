package routers

import (
	"api-gateway/controllers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func SetupRouter(router *gin.Engine) {
	cusCon := controllers.InitCustomerController()

	customerV1 := router.Group("/customer/v1/")
	{
		customerV1.POST("otps/request", cusCon.OtpRequest)
		customerV1.POST("otps/validate", cusCon.OtpValidate)
		customerV1.POST("customers", cusCon.CustomerRegister)
	}

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Api Gateway is running.",
		})
	})
}
